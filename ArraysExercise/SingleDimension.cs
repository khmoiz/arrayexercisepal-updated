﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysExercise
{
    class SingleDimension
    {
        public SingleDimension()
        {
            Console.WriteLine("Single Dimension Array");

            // Create a 9 element, single dimension array of integers and assign its contents in the same line of code.
            // (Use random numbers for the contents.)

            // Access the 5th element and change its value to 9.

            // Access the 9th element and change its value to 5.

            // Access the 3rd element and change its value to 8.


            // Use a for loop to print out the elements of the array by iterating through it.

            // Print the number of elements in the array.

            // Print the number of dimensions in the array.

            // Create an empty single dimension string array of length 5.

        }
    }
}
