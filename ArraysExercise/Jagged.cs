﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysExercise
{
    class Jagged
    {
        public Jagged()
        {
            Console.WriteLine("Jagged Arrays");
            int[][] jagged = new int[8][];
            Random rnd = new Random();

          
            for (int j = 0; j < 8; j++)
            {
                int len = rnd.Next(1, 4);
                jagged[j] = GetJaggedArray(len);
                Console.WriteLine("Length: " + jagged[j].Length);

            }
            DisplayJaggedArrayForEach(jagged);
            // Create a jagged int array with enough space for 8 arrays, leaving the jagged array empty.

            // Set each element of the jagged array to be a single dimensional int array of length 1, 2, or 3. 
            // Fill each array with numbers on the same lines.

            // Print the number of elements and number of dimensions in the jagged array.

            // Use loops to print every element of every array in the jagged array.

            Console.ReadKey();
        }



        void DisplayJaggedArrayForEach(int[][] jagged)
        {

            for (int x = 0; x < 8; x++)
            {
                int[] temp = new int[jagged[x].Length];
                temp = jagged[x];
                foreach (int value in temp)
                {
                    Console.Write(value + " ");
                }
                Console.WriteLine();
            }

        }

        void DisplayJaggedArrayFor(int[][] jagged)
        {

            for (int x = 0; x < 8; x++)
            {

                for (int z = 0; z < jagged[x].Length; z++)
                {
                    Console.Write(jagged[x][z] + " ");
                }
                Console.WriteLine("");
            }

        }

        int[] GetJaggedArray(int len)
        {
            int[] newArray = new int[len];
            Random newVal = new Random();
            for (int x = 0; x < len; x++)
            {
                newArray[x] = newVal.Next(1, len * 10);
            }

            return newArray;
        }


    }
}
