﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysExercise
{
    class MultiDimension
    {
        public MultiDimension()
        {
            Console.WriteLine("MultiDimensional Array");

            // Create a 2 dimensional string array with the dimensions (4 x 3), and leave it empty.

            // Create a 5 dimensional double array with the dimensions (5x9x2x5x4), and leave it empty.

            // Print the number of elements and number of dimensions of the above array.

            // Create a 2 dimensional int array of dimensions (2x2) and set each element of the array after making an empty one.

            // Use for loops to print each of the 4 elements in the above array.

            // Bonus: Create a 2 dimensional int array with the dimensions (3x2) and initialize it with random numbers //in the same line//.
        }
    }
}
